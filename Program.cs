﻿public class Program
{
    public static void Main(string[] args)
    {


        Console.Write($"\n\n\t\tsite is being updated...");
        using (var progress = new ProgressBar()) {
        	for (int i = 0; i <= 100; i++) {
        		progress.Report((double) i / 100);
        		Thread.Sleep(50);
        	}
        }
        Console.WriteLine("done" );
        Console.BackgroundColor = ConsoleColor.DarkGray;

        Console.ForegroundColor = ConsoleColor.DarkGreen;
        Console.Clear();
        Console.WriteLine($" \n\n\t\t\t       ☪️  Assalamu alekum  ☪️       ");
        Console.WriteLine($" \t \t \t Quron 📖 Downloaderga xush kelibsiz  🙂 ");

       
        bool during = true;
                            
        while (during)
        {
            Console.WriteLine($" \n 🛒  Qaysi surani yuklamoqchisiz, tartib raqamini kiriting :");

            try
            {
                var numberOfSurah = int.Parse(Console.ReadLine());

                QuranDownloader qd = new QuranDownloader();

                qd.OnDownload += OndownloadHandle;
                qd.EndDownload += EndDownloadHandle;

                qd.Download(numberOfSurah);

            }
            catch (Exception)
            {
                Console.WriteLine($"siz raqam kiritmadingiz");
                goto key;
            }


        key:
            Console.WriteLine($"Davom Etasizmi (raqamni tanlang) :");
            Console.WriteLine($"1 : 'ha' ,    2 : 'yoq'");

            during = int.Parse(Console.ReadLine()) == 1 ? true : false;

        }

        Console.WriteLine($" Salomat bo'ling 👋");
        Console.ReadLine();


        // == here methods for event
        void OndownloadHandle(object sender, DownloadEvenArgs e)
        {
            Console.WriteLine($"\n {e.NumberOfSurah} raqamli surani yuklash boshlandi \n");
        }

        void EndDownloadHandle(object sender, DownloadEvenArgs e)
        {
            Console.WriteLine($"\n {e.NumberOfSurah} raqamli surani yuklash {e.EndTime}da  yakunlandi, ");
            // Console.WriteLine($" sura {e.EndTime} da yuklandi ");
            Console.WriteLine($"sura quyidagi papkada joylashgan");
            Console.WriteLine($"{e.PathOfUploadedFile} \n");

        }
    }


}
 






 